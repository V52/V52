# -*- coding: utf-8 -*-
"""
Setup file for V52
"""


from setuptools import setup

setup(name='v52',
      version='1.0',
      description='V52 - a tool to work with measurements and simulations',
      url='https://gitlab.windenergy.dtu.dk/V52/V52',
      author='MIKF',
      author_email='mikf@dtu.dk',
      license='GNU GPL',
      packages=['v52'],
      install_requires=[
               'windrose',
               'mysql-connector-python-rf',
               # 'mysql-connector'
              ],
      zip_safe=True)
