from v52 import V52
import os

if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_sql
    except:
        user = input('Username: ')
        password = input('Password: ')

    start = (2018, 3, 1, 1, 0)
    stop = (2018, 3, 5, 1, 0)

    calc_dir = __file__[:-3]
    if not os.path.exists(calc_dir):
        os.makedirs(calc_dir)
    fig_path = os.path.join(calc_dir, 'Windrose.png')
    
    v52 = V52(calc_dir=calc_dir)
    v52.get_time_series(start, stop)
    v52.get_data_from_SQL(user, password, tablename='calmeans')
    x = v52.data['calmeans'].Wdir_41m
    y = v52.data['calmeans'].Wsp_44m
    v52.plot_windrose(x, y, fig_path=fig_path)
