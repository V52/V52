from v52 import V52
import os

if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_sql
    except:
        user = input('Username: ')
        password = input('Password: ')

    start = (2018, 3, 1, 1, 0)
    stop = (2018, 3, 1, 5, 0)

    calc_dir = __file__[:-3]
    if not os.path.exists(calc_dir):
        os.makedirs(calc_dir)
    file_path = os.path.join(calc_dir, 'data.pickle')
    tablenames = ['calmeans', 'calmaxs']

    v52 = V52(calc_dir=calc_dir)
    v52.get_time_series(start, stop)
    logcial_statements = ['`SWsp_70m`>8',
                          '`SWsp_70m`<10']
    for tn in tablenames:
        v52.get_data_from_SQL(user, password, v52.time_series, tablename=tn,
                              logical_statements=logcial_statements)
    v52.dump_data(file_path)

    # Look at the data by e.g. plotting all channels with a standard deviation 
    # between 2 and 4:
    df = v52.data['calmeans']
    stds = df.describe().loc['std']
    df.plot(y=stds[(stds > 2) & (stds < 4)].index)
    print(df['SWsp_70m'])