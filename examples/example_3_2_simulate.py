from v52 import V52
import os


if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_jess
    except:
        user = input('Username: ')
        password = input('Password: ')

    calc_dir = os.path.join(os.path.dirname(__file__), 'example_3')
    v52 = V52(calc_dir)
    v52.get_model()
    sim_resource = 2
    if sim_resource == 2:
        v52.copy_model_to_cluster(user, password,host='jess.dtu.dk')
#    v52.run_simulation(user, password, sim_resource)
