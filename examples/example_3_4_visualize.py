from v52 import V52
import os
import matplotlib.pyplot as plt

if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_sql
    except:
        user = input('Username: ')
        password = input('Password: ')
    start = (2018, 2, 1, 1, 0)
    stop = (2018, 2, 2, 1, 0)
    calc_dir = os.path.join(os.path.dirname(__file__), 'example_3')
    res_db = 'v52_simulation'
    v52 = V52(calc_dir)
    v52.get_time_series(start, stop)
    v52.get_stats_from_files()
    v52.get_data_from_SQL(user, password, tablename='calmeans')
    x = v52.data['calmeans'].ROT
    y = v52.stats['lc1_wsp']['mean'].Omega
    plt.scatter(x, y)
    plt.show()
