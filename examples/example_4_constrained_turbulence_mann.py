from v52 import V52
import os
from v52.turb_plot import read_bat, write_pdap_script
import pandas as pd

if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_sql
    except:
        user = input('Username: ')
        password = input('Password: ')

    csim_exe_path = r"C:\Sandbox\ConTurb\Conturb\csimu2_win\csimu2.exe"
    calc_dir = os.path.join(os.path.dirname(__file__), 'example_4')
    if not os.path.exists(calc_dir):
        os.makedirs(calc_dir)
    LC_script_path = __file__
    res_db = 'v52_simulation'
    v52 = V52(calc_dir)
    v52.time_series = ['201802010100', '201803010100']
    file_path = os.path.join(calc_dir, 'data.p')
    if not os.path.exists(file_path):
        v52.get_data_from_SQL(user, password)
        v52.dump_data(file_path)
    else:
        v52.get_data_from_file(file_path)
    v52.csim_exe_path = csim_exe_path
    v52.get_model()
    LCs = [
            {
                'name': 'conturb_mann',
                'script': LC_script_path
             },
            ]
    v52.init_LCs(LCs)
    v52.resolve_LCs(LCs)
    print(str(len(v52.htcs_created))+' new htc-files created.')
    for ts in v52.time_series:
        turb_dir = os.path.join(v52.hawc2_dir, 'turb', LCs[0]['name'], ts)
        bat_path = os.path.join(turb_dir, "constr.bat")
        pdap_script_path = os.path.join(turb_dir, "pdap_turb_plot.py")
        csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path = read_bat(bat_path)
        df = pd.read_csv(constr_path,sep=';',names = ['x','y','z','comp','wsp'])
        write_pdap_script(df, pdap_script_path, csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path)

'''
Load case definition. This content could go into a seperate file if that is
prefered. The name of the function run_script() is mandatory but for the rest
of the used functions used the naming is free.
'''
from v52.turbulence import (create_conturb_input_mann, get_ID, create_constr,
                            create_csim_bat, update_htc, run_conturb)


def run_script(htc_path, LC, data, ts, v52):
    data = v52.data['caldata_{}_{}_50hz']
    data = data[data.Name == ts]
    turb_dir = os.path.join(v52.hawc2_dir, 'turb', LC['name'], ts)
    if not os.path.exists(turb_dir):
        os.makedirs(turb_dir)
    x, y, z, us, N, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, wsp = create_conturb_input_mann(v52.csim_exe_path, htc_path, data, ts)
    ID = get_ID(nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed)
    constr_path = os.path.join(turb_dir, 'constr.dat')
    csim_bat_path = os.path.join(turb_dir, 'constr.bat')
    create_constr(constr_path, N, x, y, z, N*['u'], us)
    create_csim_bat(turb_dir, v52.csim_exe_path, csim_bat_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, constrained=True)
    update_htc(htc_path, ID, ts, wsp)
    run_conturb(v52.hawc2_dir)
