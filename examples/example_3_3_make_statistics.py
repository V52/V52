from v52 import V52
import os


if __name__ == '__main__':
    try:
        import password
        user = password.user
        password = password.pass_sql
    except:
        user = input('Username: ')
        password = input('Password: ')

    calc_dir = os.path.join(os.path.dirname(__file__), 'example_3')
    res_db = 'v52_simulation'
    v52 = V52(calc_dir)
    v52.make_stats()
    v52.get_stats_from_files()
    v52.upload_to_SQL(user, password, res_db)
    '''
    Now try to look at the results in Rodeo
    http://rodeo.dtu.dk/rodeo/GraphicsPage.aspx?&Project=229&Year=2018&Month=2&Day=1&PageName=1&Rnd=773057&ScrollTop=0 
    '''
