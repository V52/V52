# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 14:01:44 2018

@author: mikf
"""
import pandas as pd
import os

def read_bat(bat_path):
    with open(bat_path, 'r') as f:
        cont = f.read().split(' ')
    if len(cont) == 13:
        csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path = cont
    else:
        Warning('Spaces in path names are not supported')
    return csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path
    
def write_pdap_script(df, pdap_script_path, csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path):
    c = ''    
    c += "name = '{}'\n".format(os.path.basename(turb_path+"u.bin"))
    c += 'if name not in model:\n'
    c += '  ds  = OpenTurbulence(filename= r"{}",\n'.format(turb_path+"u.bin")
    c += '                dim=({},{},{}),\n'.format(nx,ny,nz)
    c += '                distance=({},{},{}), \n'.format(sx,sy,sz)
    c += '                transport_speed=0, \n'
    c += '                mean_wsp=0, \n'
    c += '                center_position=(0, 0, -100))\n'
    c += 'else:\n'
    c += '  ds = model(name)\n'
    c += 'constr_path = r"{}"\n'.format(constr_path)
    c += "df = pd.read_csv(constr_path,sep=';',names = ['x','y','z','comp','wsp'])\n"
    c += 'y = 16\n'
    c += 'z = 16\n'
    c += "wsp = df['wsp'][df['z'] == z].values\n"
    c += "x = df['x'][df['z'] == z].values\n"
    c += "PlotData(None, x, wsp,style='r.',markersize=10)\n"
    c += "Plot(ds('(x,{},{})'.format(y,z)))\n"
    c += "Cell2Image(cell=('New',(0,0)), filename=r'{}', size=(15, 10), font_size=7.0, resolution=600)\n".format(os.path.join(os.path.dirname(pdap_script_path),'check_turb.png'))
    with open(pdap_script_path,'w') as h:
        h.write(c)

if __name__ == '__main__':
    bat_path = r"C:\Sandbox\Git\V52\turb\constr.bat"
    pdap_script_path = r"C:\Sandbox\Git\V52\turb\pdap_turb_plot.py"
    csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path = read_bat(bat_path)
    df = pd.read_csv(constr_path,sep=';',names = ['x','y','z','comp','wsp'])
    write_pdap_script(df, pdap_script_path, csimu2_path, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, turb_path, constr_path)
    