# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 16:52:07 2018

@author: mikf
"""
#import mysql.connector
import pandas as pd
from datetime import date, timedelta
import numpy as np
import pickle
from scipy import interpolate
import os
from shutil import copytree, copy
from wetb.utils.cluster_tools.ssh_client import SSHClient
from wetb.prepost.windIO import LoadResults
from v52.SQL import SQL_read_table


def get_time_series(start, stop):
    lst = []
    start, hh1, mm1 = date(start[0], start[1], start[2]), start[3], start[4]
    stop, hh2, mm2 = date(stop[0], stop[1], stop[2]), stop[3], stop[4]
    delta = stop - start
    for i in range(delta.days + 1):
        YYYY, MM, DD = str(start + timedelta(i)).split('-')
        if delta.days == 0:
            for mm in range(int(mm1/10), 6):
                hh = hh1
                lst.append(_ts_string(YYYY, MM, DD, hh, mm))
            for hh in range(hh1+1, hh2):
                for mm in range(0, 6):
                    lst.append(_ts_string(YYYY, MM, DD, hh, mm))
            for mm in range(0, int(mm2/10)):
                hh = hh2
                lst.append(_ts_string(YYYY, MM, DD, hh, mm))
        elif i == 0:
            for mm in range(int(mm1/10), 6):
                hh = hh1
                lst.append(_ts_string(YYYY, MM, DD, hh, mm))
            for hh in range(hh1+1, 24):
                for mm in range(0, 6):
                    lst.append(_ts_string(YYYY, MM, DD, hh, mm))
        elif i == delta.days:
            for hh in range(0, hh2):
                for mm in range(0, 6):
                    lst.append(_ts_string(YYYY, MM, DD, hh, mm))
            for mm in range(0, int(mm2/10)):
                hh = hh2
                lst.append(_ts_string(YYYY, MM, DD, hh, mm))
        else:
            for hh in range(0, 24):
                for mm in range(0, 6):
                    lst.append(_ts_string(YYYY, MM, DD, hh, mm))
    print('Number of 10-minute series queried: ' + str(len(lst)))
    return lst


def _ts_string(YYYY, MM, DD, hh, mm):
    return YYYY+MM+DD+'{:02d}{:02d}'.format(hh, mm*10)


def get_data_from_SQL(
        user, password, timestamps, database, tablename, rowlimit,
        cols, host, logical_statements=[]):
    dic = {}
    for n, name in enumerate(timestamps):
        YYYY = name[:4]
        MM = name[4:6]
        table_name = tablename.format(YYYY, MM)
        if table_name not in dic:
            dic[table_name] = {'Name': [name]}
        else:
            dic[table_name]['Name'].append(name)
    for n, table_name in enumerate(dic):           
        inp = dict(
                user=user,
                password=password,
                host=host,
                database=database,
                SQLrowlimit=rowlimit,
                cols=cols,
                table_name=table_name,
                rows=dic[table_name],
                logical_statements=logical_statements,
                )
        df = SQL_read_table(inp)
        if n == 0:
            df_out = df
        else:
            df_out = df_out.append(df, ignore_index=True)
    return df_out


def stat_val(variables, oper, df):
    df.dropna(inplace=True)
    for i, var in enumerate(variables):
        oper = oper.replace('var{:d}'.format(i+1),
                            "df['{}']".format(variables[i]))
    string = 'import numpy as np\n' + 'value = ' + oper
    namespace = {'df': df}
    try:
        exec(string, namespace)
        value_series = pd.Series(len(df)*[namespace['value']])
    except Warning:
        value_series = pd.Series(len(df)*[np.nan])
    return value_series


def bin_data(data, bin_key, bin_type, bin_value, mx, mn, index, aggdata):
    data = data[~np.isnan(data[bin_key])]
    if mx is None:
        mx = (int(max(data[bin_key])*1000)+1)/1000
    if mn is None:
        mn = int(min(data[bin_key])*1000)/1000
    if bin_type == 'no':
        bin_size = (mx-mn)/(bin_value-1)
    elif bin_type == 'size':
        bin_size = bin_value
    oom = int(np.log10(bin_size))
    bins = np.arange(mn, mx+1.0*bin_size, bin_size)
    bin_strings = ['[{:=5.2f}, {:=5.2f}['.format(bins[x]/10**oom,
                   bins[x+1]/10**oom) for x in range(len(bins)-1)]
    bin_strings[-1] = bin_strings[-1][:-1]+']'
    if index is None:
        index = 'Bins [{}*10**{:d}]'.format(bin_key, -oom)
    data[index] = data[bin_key].apply(
            _map_bin, bins=bins, bin_strings=bin_strings)
    grouped = data.groupby(index)
    grouped_data = grouped.agg(aggdata)
    grouped_data = grouped_data.reindex(bin_strings)
    return grouped_data


def _map_bin(x, bins, bin_strings):
    kwargs = {}
    if x == max(bins):
        kwargs['right'] = True
    bindex = np.digitize([x], bins, **kwargs)[0]
    if x < min(bins):
        pass
    elif x > max(bins):
        pass
    else:
        return bin_strings[bindex-1]


def dump_data(data, file_path):
    with open(file_path, 'wb') as f:
        pickle.dump(data, f)


def get_data_from_file(file_path):
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    for tablename in data:
        data[tablename] = data[tablename].astype('float')
        data[tablename]['Name'] = data[tablename]['Name'].astype('str')
    return data


def ntf(data, bin_name, x, bin_stat, y, f1, smooth_from, fill_value):
        ntf = data[bin_name][x][bin_stat]/(
                data[bin_name][y][bin_stat]*f1)
        ntf[smooth_from:len(ntf)] = ntf[smooth_from]
        f = interpolate.interp1d(data[bin_name][x][bin_stat].values,
                                 ntf, fill_value=fill_value)
        return f, ntf


def get_model(calc_dir, repo_dir, repo, git_sha, force=False):
    if not os.path.exists(calc_dir):
        os.makedirs(calc_dir)
    git_checkout_bat_path = os.path.join(calc_dir, 'checkout.bat')
    content = 'cd {}\n'.format(calc_dir)
    if (not os.path.exists(repo_dir)) or force:
        content += 'git clone {} {}\n'.format(repo, 'V52')
    else:
        content += ''
    content += 'cd {}\n'.format(repo_dir)
    content += 'git checkout {}\n'.format(git_sha)
    with open(git_checkout_bat_path, 'w') as f:
        f.write(content)
    os.system(git_checkout_bat_path)


def copy_to_calc_dir(repo_dir, hawc2_dir, htc_template_dir, res_dir,
                     force=False):
    if not os.path.exists(hawc2_dir):
        os.makedirs(hawc2_dir)
    srcs = os.path.join(repo_dir, 'hawc2')
    files = os.listdir(srcs)
    for f in files:
        src = os.path.join(repo_dir, 'hawc2', f)
        dst = os.path.join(hawc2_dir, f)
        if not os.path.exists(dst):
            copy(src, dst)
    src = os.path.join(repo_dir, 'control')
    dst = os.path.join(hawc2_dir, 'control')
    if not os.path.exists(dst):
        copytree(src, dst)
    src = os.path.join(repo_dir, 'data')
    dst = os.path.join(hawc2_dir, 'data')
    if not os.path.exists(dst):
        copytree(src, dst)
    if not os.path.exists(htc_template_dir):
        os.makedirs(htc_template_dir)
    if not os.path.exists(res_dir):
        os.makedirs(res_dir)


def transfer_model(username, password, localpath, remotepath, host='gorm.risoe.dk'):
    client = SSHClient(host=host, port=22, username=username, password=password)
    file_lst = []
    for file in os.listdir(localpath):
        if os.path.isfile(os.path.join(localpath, file)):
            remote_file_path = os.path.join(remotepath, file).replace("\\", "/")
            try:
                client.execute("rm -f %s" % remote_file_path)
            except:
                print('Could not remove file. Either it is not a file or it does not exist')
            file_lst.append(file)
    print('Uploading files...')
    client.upload_files(localpath, remotepath, file_lst)
    print('Done')


def load_sel(sel_path):
    res = LoadResults(os.path.dirname(sel_path), os.path.basename(sel_path))
    df = res.sig2df()
    meta_df = res.ch_df.transpose()
    meta_df.columns = list(df)
    meta_df = meta_df.astype('str')
    meta_df.insert(0, 'parameter', meta_df.index)
    return df, meta_df


if __name__ == '__main__':
    start = (2018, 10, 11, 0, 0)
    stop = (2018, 10, 12, 8, 0)

    lst = get_time_series(start, stop)
    print(lst)