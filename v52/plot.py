# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 13:16:25 2018

@author: mikf
"""
import matplotlib.pyplot as plt
from windrose import WindroseAxes


def plot_windrose(x, y, opt_bar, opt_leg, fig_path):
    ax = WindroseAxes.from_ax()
    ax.bar(x, y, **opt_bar)
    ax.set_legend(**opt_leg)
    if fig_path:
        plt.savefig(fig_path)

def plot(xs, ys, line_opts, plot_opts, fig_path):
    for x, y, opt in zip(xs, ys, line_opts):
        plt.plot(x , y, **opt)
    if 'xlabel' in plot_opts:
        plt.xlabel(plot_opts['xlabel'])
    if 'ylabel' in plot_opts:
        plt.ylabel(plot_opts['ylabel'])
    if fig_path:
        plt.savefig(fig_path)  

