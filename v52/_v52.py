# -*- coding: utf-8 -*-
from v52.utils import (get_time_series, get_data_from_SQL, stat_val, bin_data,
                       dump_data, get_data_from_file, ntf, get_model,
                       copy_to_calc_dir, transfer_model, load_sel)
from v52.plot import plot_windrose, plot
from v52.create_turb import create_turb
import os
from shutil import copy
from wetb.hawc2.htc_file import HTCFile
import numpy as np
import importlib.util
from wetb.hawc2.simulation_resources import (GormSimulationResource,
                                             PBSClusterSimulationHost)
from wetb.hawc2 import simulation
from wetb.hawc2.simulation import Simulation
from wetb import gtsdf
import time
import pandas as pd
import pickle
from v52.SQL import (SQL_read_table, SQL_write_table)
import datetime
from v52.create_hash import get_hash


class V52(object):
    """
    Main object for operating on V52 measurement data and
    performing simulations.
    """

    def __init__(self, calc_dir='default', 
                 htc_master=r'htc_master_mikf\DTU_master.htc'):
        self.binned_data = {}
        if calc_dir == 'default':
            calc_dir = os.path.join(os.getcwd(), '..', 'temp')
        self.calc_dir = calc_dir
        self.repo_dir = os.path.join(calc_dir, 'V52')
        self.hawc2_dir = os.path.join(self.calc_dir, 'HAWC2')
        self.res_dir = os.path.join(self.hawc2_dir, 'res')
        self.htc_template_path = os.path.join(self.repo_dir, htc_master)
        self.htc_template_dir = os.path.join(self.hawc2_dir, 'htc')
        self.info = {}
        self.data = {}
        self.logpath = os.path.join(calc_dir, 'v52.log')
        self.loglst = []
        self.htc_dir = os.path.join(self.hawc2_dir, 'htc')

    def log(self, text):
        with open(self.logpath, 'w') as f:
            f.write(text)
        self.loglist = []

    def get_time_series(self, start, stop):
        self.time_series = get_time_series(start, stop)

    def get_time_series_from_data(self, tablename):
        if hasattr(self, 'data'):
            self.time_series = list(set(self.data[tablename].Name))
        else:
            print('object has no data. please load data from DB or file first')

    def get_time_series_from_file(self, file):
        with open(file, 'r') as f:
            content = f.readlines()
        self.time_series = [str(c.strip()) for c in content]

    def get_data_from_SQL(
            self, user, password, timestamps='default', database='v52_wtg',
            tablename='caldata_{}_{}_50hz', rowlimit=None,
            cols='all', host='10.40.20.14', logical_statements=[]):
        if timestamps == 'default':
            timestamps = self.time_series
        self.data[tablename] = get_data_from_SQL(user, password, timestamps,
                 database, tablename, rowlimit, cols, host,
                 logical_statements=logical_statements)
        self.correct_name()
        self.len = len(self.data[tablename])
        self.get_time_series_from_data(tablename)

    def _add_column(self, name, values, tablename):
        if name not in self.data[tablename]:
            self.data[tablename][name] = values
        else:
            print('*Column already exists. Overwriting is not supported.\n')

    def add_stat_val(self, name, variables, oper, tablename):
        for n, ts in enumerate(self.time_series):
            df = self.data[tablename][self.data[tablename].Name == ts]
            value_series = stat_val(variables, oper, df)
            if n == 0:
                values = value_series
            else:
                values = values.append(value_series, ignore_index=True)
        self._add_column(name, values)

    def plot_windrose(self, x, y, opt_bar={'normed': True, 'opening': 0.8,
                                           'edgecolor': 'white'},
                      opt_leg={'decimal_places': 4, 'loc': (-0.1, -0.1)},
                      fig_path=None):
        plot_windrose(x, y, opt_bar, opt_leg, fig_path)

    def dump_data(self, file_path):
        dump_data(self.data, file_path)

    def get_data_from_file(self, file_path):
        self.data = get_data_from_file(file_path)
        self.correct_name()

    def correct_name(self):
        for tablename in self.data:
            self.data[tablename].Name = self.data[tablename].Name.str.slice(stop=12)
            self.get_time_series_from_data(tablename)

    def bin_data(self, bin_key, bin_type, bin_value, tablename, mx=None, mn=None,
                 index=None, aggdata=['mean', 'max', 'min', 'std', 'count'],
                 bin_name=None):
        if not bin_name:
            bin_name = bin_key
        binned_data = bin_data(self.data[tablename], bin_key, bin_type, bin_value, mx, mn, index, aggdata)
        self.binned_data[bin_name] = binned_data

    def get_ntf(self, tablename, x='Wsp_44m', y='sa_speed', f1=1.445, smooth_from=-1,
            bin_type='size', bin_value=0.5, index=None, bin_stat='mean',
            bin_name='ntf', fill_value='extrapolate'):
        self.bin_data(x, bin_type, bin_value, tablename, index=index,
                      aggdata=[bin_stat], bin_name=bin_name)
        self.f, self.ntf = ntf(self.binned_data, bin_name, x, bin_stat, y, f1,
                               smooth_from, fill_value)

    def plot(self, xs=[[]], ys=[[]], line_opts=[{}], plot_opts={},
             fig_path=None):
        plot(xs, ys, line_opts, plot_opts, fig_path)

    def get_model(self,
                  repo='https://gitlab-internal.windenergy.dtu.dk/V52/V52_model.git',
                  git_sha='4c3ce0e6e676bc4e7fed604180039dc7ac594f9c',
                  force=False):
        get_model(self.calc_dir, self.repo_dir, repo, git_sha, force)
        copy_to_calc_dir(self.repo_dir, self.hawc2_dir, self.htc_template_dir,
                         self.res_dir, force)

    def init_LCs(self, LCs, force=False):
        self.info['LCs'] = LCs
        for LC in LCs:
            LC_htc_dir = os.path.join(self.hawc2_dir, 'htc', LC['name'])
            LC['lc_htc_dir'] = LC_htc_dir
            if not os.path.exists(LC_htc_dir):
                os.makedirs(LC_htc_dir)
            src = self.htc_template_path
            for ts in self.time_series:
                dst = os.path.join(LC_htc_dir, ts+'.htc')
                if os.path.exists(dst) and not force:
                    pass
                else:
                    copy(src, dst+'_temp')

    def resolve_LCs(self, LCs, force=False):
        htcs_created = []
        for ts in self.time_series:
            dic = {}
            for tablename in self.data:
                tndata = self.data[tablename]
                dic[tablename] = tndata[tndata.Name == ts]
            for LC in LCs:
                LC_htc_dir = LC['lc_htc_dir']
                htc_path = os.path.join(LC_htc_dir, ts+'.htc')
                htc_path_temp = os.path.join(LC_htc_dir, ts+'.htc_temp')
                if os.path.exists(htc_path) and not force:
                    pass
                else:
                    spec = importlib.util.spec_from_file_location("run_script", LC['script'])
                    module = importlib.util.module_from_spec(spec)
                    spec.loader.exec_module(module)
                    module.run_script(htc_path_temp, LC, dic, ts, self)
                    os.rename(htc_path_temp, htc_path)
                    htcs_created.append(htc_path)
        self.htcs_created = htcs_created
        return htcs_created



    def copy_model_to_cluster(self,username, password, host='gorm.risoe.dk'):
        localpath = self.hawc2_dir
        remotepath = r'wine_exe/win32/'
        transfer_model(username, password, localpath, remotepath, host)
        folders = ['data','control']
        for folder in folders:
            lp = os.path.join(localpath,folder)
            rp = os.path.join(remotepath,folder)
            transfer_model(username, password, lp,rp, host)

        
        
    def run_simulation(self, user,password, sim_resource, skip = []):
        modelpath = self.hawc2_dir
        htc_dir = os.path.join(modelpath,'htc')
        for root, dirs, files in os.walk(htc_dir):
            for file in files:
                if file.endswith('.htc') and file not in skip:
                    htc_path = os.path.join(root,file)
                    htc = HTCFile(htc_path)
                    if sim_resource == 1:
                        htc.simulate("hawc2mb.exe")
                    elif sim_resource == 2:
                        sim = Simulation(modelpath, htc.filename, "hawc2mb.exe", False)
                        resource = GormSimulationResource(user, password)
                        sim.host = PBSClusterSimulationHost(sim, resource)
                        sim.start()
                        while sim.status not in [simulation.CLEANED, simulation.ERROR]:
                            resource.update_resource_status()
                            sim.show_status()
                            time.sleep(10)
                    else:
                        raise ValueError(f'Unrecognized simulation resource "{sim_resource}"')

    def make_stats(self, skip = []):
        ll = ['min', 'max', 'mean', 'std', 'metadata']
        for root, dirs, files in os.walk(self.res_dir):
            LC = os.path.basename(root)
            for f in files:
                if f.endswith('.sel') and f not in skip:
                    sel_path = os.path.join(root, f)
                    Name = f[:-4]
                    stats_paths = [os.path.join(root, Name+'_{}.p'.format(v)) for v in ll]
                    if not np.array([os.path.exists(p) for p in stats_paths]).any():
                        df, info_df = load_sel(sel_path)
                        stats = df.describe()
                        unique_dic = {
                        'LC': LC,
                        'Name': Name,
                        }
                        ID = get_hash(**unique_dic)
                        for v in ll:
                            stats_path = os.path.join(root, Name+'_{}.p'.format(v))
                            if v != 'metadata':
                                df = pd.DataFrame(stats.loc[v]).transpose().reset_index()
                                df.pop('index')
                            else:
                                df = info_df
                            df.insert(0, 'Name', Name)
                            df.insert(1, 'LC', LC)
                            df.insert(2, 'ID', ID)
                            with open(stats_path, 'wb') as g:
                                pickle.dump(df, g)
                            
    def get_stats_from_files(self, skip = []):
        self.stats = {}
        ll = ['min', 'max', 'mean', 'std', 'metadata']
        for root, dirs, files in os.walk(self.res_dir):
            LC = os.path.basename(root)
            for f in files:
                if f.endswith('.sel') and f not in skip:
                    if LC not in self.stats:
                        self.stats[LC] = {}
                    Name = f[:-4]
                    for v in ll:
                        stats_path = os.path.join(root, Name+'_{}.p'.format(v))
                        if v != 'metadata':
                            with open(stats_path, 'rb') as g:
                                df = pickle.load(g)
                            if v not in self.stats[LC]:
                                self.stats[LC][v] = df
                            else:
                                self.stats[LC][v] = self.stats[LC][v].append(df, ignore_index=True, sort=True)

    def upload_to_SQL(self, user, password, res_db, host='10.40.20.14',
                      skip = []):
        stat_dir = os.path.join(self.hawc2_dir,'res')
        tables = ['max', 'mean', 'min', 'std', 'metadata']
        for root, dirs, files in os.walk(stat_dir):
            LC = os.path.basename(root)
            for f in files:
                for t in tables:
                    end = '_{}.p'.format(t)
                    if f.endswith(end) and f not in skip:
                        if t == 'std':
                            t = 'stdv'
                        table_name = 'cal'+t+'s'
                        stats_path = os.path.join(root, f)
                        with open(stats_path, 'rb') as g:
                            df2 = pickle.load(g)
                        SQLdic = {
                            'user': user,
                            'password': password,
                            'host': host,
                            'database': res_db,
                            }
                        if t == 'metadata':
                            SQLdic['table_name'] = 'run_def'
                            df2 = self._get_run_def(df2, SQLdic)
                        else:
                            SQLdic['table_name'] = table_name
                        SQLdic['df'] = df2
                        ok = self.check_db(SQLdic)
                        if ok:
                            SQL_write_table(SQLdic, if_exists='append')
        self.log('\n'.join(list(set(self.loglst))))

    def _get_run_def(self, df2, SQLdic):
        dic = {}
        try:
            df3 = SQL_read_table(SQLdic)
            for key in list(df3):
                dic[key] = [df3[key].values[-1]]
        except Warning('run_def table does not exist.'):
            pass
        names = list(set(df2.Name.values))
        dt = [datetime.datetime.strptime(name, '%Y%m%d%H%M') for name in names]
        dic['Name'] = names
        dic['start_dt'] = dt
        dic['stop_dt'] = [x + datetime.timedelta(minutes=10) for x in dt]
        dic['day_time'] = [x.hour + x.minute / 60 for x in dt]
        dic['run_index'][0] += 1
        dic['seq_number'][0] += 1
        df = pd.DataFrame(dic)
        return df

    def check_db(self, SQLdic):
        df2 = SQLdic['df']
        try:
            df3 = SQL_read_table(SQLdic)
            if set(list(df3)) == set(list(df2)):
                if df3.Name.isin(df2.Name).any():
                    ok = False
                    logstr = 'Record already found in DB, not writing'
                    self.loglst.append(logstr)
                else:
                    
                    ok = True
            else:
                ok = False
                logstr = 'Simulation signals does not match what is in DB\n'
                logstr += 'DB is missing columns:\n {}'.format(set(df2)-set(df3))
                self.loglst.append(logstr)
        except:
            ok = True
        return ok

    def df_to_gtsdf(self, df, filename='df.hdf5', skip_cols=2, name='df',
                    time_col='Name'):
        data = np.array(df).T[skip_cols:]
        gtsdf.save(filename, data.T, name=name,
                   attribute_names=list(df)[skip_cols:],
                   time=pd.to_numeric(df[time_col]), dtype=np.float64)

    def get_completed_htcs(self):
        LCs = self.info['LCs']
        dic = {}
        for LC in LCs:
            LC_htc_dir = os.path.join(self.hawc2_dir, 'htc', LC['name'])
            files = os.listdir(LC_htc_dir)
            dic[LC['name']] = [x[:-4] for x in files]
        self.info['LC_files'] = dic
        return dic
    
    def create_turb(self, turb_gen, lc, turb_type='mann'):
        LCs = self.info['LCs']
        for LC in LCs:
            create_turb(self.hawc2_dir, turb_gen, LC['name'],
                        turb_type=turb_type)


if __name__ == '__main__':
    start = (2018, 2, 1, 1, 0)
    stop = (2018, 2, 2, 1, 0)
    import password
    user = password.user
    pass_gorm = password.pass_gorm
    password = password.pass_sql
#    rowlimit = 100
#    name = 'TI'
#    variables = ['Wsp_44m']
#    oper = 'np.std(var1)/np.mean(var1)'
#    fig_path = 'Windrose.png'
#    plot_vars = ['Wdir_41m', 'TI']
    file_path = 'data5.p'
#    tablename = 'calmeans'
#    tablenames = ['calmeans', 'calstdvs']
    res_db = 'v52_simulation'
#    tablename = 'calstdvs'
    tablenames = ['caldata_{}_{}_50hz']
#    bin_key = 'Wsp_44m'
#    bin_key = 'TI'
#    bin_size = 0.5
#    bin_type = 'size'
#    bin_value = 0.5
#    bin_type = 'no'
#    bin_value = 15
    LC1_script_path = r"C:\Sandbox\Git\V52\temp\LCs\LC1_wsp.py"
    v52 = V52()

    if 1:
#        v52.get_time_series(start, stop)
        v52.time_series = ['201802010100','201803010100']
        for tablename in tablenames:
            v52.get_data_from_SQL(user, password, v52.time_series,
                                  tablename=tablename)
        v52.dump_data(file_path)
    else:
        v52.get_data_from_file(file_path)
        v52.get_time_series_from_data(tablenames[0])
    LCs = [
            {
            'name': 'LC1_wsp',
            'script': LC1_script_path
             },
            ]
#    v52.init_LCs(LCs, force=False)
#    v52.resolve_LCs(LCs, force=False)
    
#    v52.copy_model_to_cluster(user, pass_gorm)
#    v52.run_simulation(user,pass_gorm,2)
#    v52.make_stats()
#    v52.upload_to_SQL(user, password, res_db)
#    v52.get_model(force=True)
    
#    v52.get_ntf(tabelnames[0])
#    v52.plot([v52.data['Wsp_44m']],[v52.f(v52.data['Wsp_44m'])],[{'label':'Sa Corected','linestyle':'None','marker':'x'}])
#    v52.bin_data(bin_key, bin_type, bin_value, mx=12, mn=0, index=None,
#                 aggdata=['mean', 'max', 'min', 'std', 'count'])
#    v52.add_stat_val(name, variables, oper)
#    v52.plot_windrose(plot_vars, fig_path=fig_path)

    if 0:
        SQLdic = {
            'user': user,
            'password': password,
            'host': '10.40.20.14',
            'database': res_db,
            'table_name': 'LC1_wsp' + '_sim_'+'mean',
            }
        df = SQL_read_table(SQLdic)
#        sim_sens = [ 'Omega',
##                    'bearing-pitch1-angle-rad'
#                    ]
#        meas_sens = [      'ROT',
##                     'pitch',
#                     ]
#        df = df[:4]
        dfm = v52.data['calmeans']
        dfm = dfm[dfm.Name.isin(df.Name)]
        df = df[df.Name.isin(dfm.Name)]
        if 1:
            v52.df_to_gtsdf(df, filename='df.hdf5', skip_cols=2, name='df',
                    time_col='Name')
            v52.df_to_gtsdf(dfm, filename='dfm.hdf5', skip_cols=1, name='dfm',
                    time_col='Name')
#            dfm.to_csv('dfm.csv')
#        import matplotlib.pyplot as plt
##        x = np.arange(0,len(df))
#        x = pd.to_numeric(df.Name) - 2.018e11      
#        n_tics = 10
#        dx = (max(x)-min(x))/(n_tics+1)
#        x_tick = np.arange(min(x), max(x)+dx,dx)
##        x_min = int(min(x))
##        x_max = int(max(x))
##        n_x = len(x)
#        for s,m in zip(sim_sens,meas_sens):
#            plt.figure()
#            plt.xticks(x_tick)
##            plt.scatter(df[s],dfm[m])
#            plt.plot(x,df[s],label='simulation')
#            plt.plot(x,dfm[m],label='measurement')
#            plt.legend()
#    #        plt.plot(x,dfm[m]/df[s])
#            plt.show()
#            