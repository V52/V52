import os
import pickle
from wetb.hawc2.htc_file import HTCFile


def create_turb(hawc2_dir, turb_gen, lc, turb_type=None):
    if not turb_type:
        turb_type='mann'
    htc_dir = os.path.join(hawc2_dir,'htc')
    turb_dir = os.path.join(hawc2_dir, 'turb', lc)
    if not os.path.exists(turb_dir):
        os.makedirs(turb_dir)

    for file in os.listdir(os.path.join(htc_dir,lc)):
        ID = file[:-4]
        turb_path = os.path.join(turb_dir, ID + '_')
        htc_path = os.path.join(htc_dir,lc,file)
        if not (os.path.exists(turb_path + 'u.bin') and
            os.path.exists(turb_path + 'v.bin') and
            os.path.exists(turb_path + 'w.bin')):
            turb_dic = get_turb_params_from_htc(htc_path)
            if turb_type=='mann':
                create_mann_turb(turb_gen, turb_dic, turb_path)
            

def create_mann_turb(csimu2_path, turb_dic, turb_path):
    csimu2_string = csimu2_path + ' '
    csimu2_string += turb_dic['turb_string']
    csimu2_string += ' ' + turb_path
    csimu2_string = csimu2_string.lower()
    print(csimu2_string)
    os.system(csimu2_string)


def create_ksec_turb(turb_dic, dic, turb_path, inp, turb_dir):
    turb_constr = inp['turb_constr']
    ksec_simu_path = inp['ksec_simu_path']
    ny = turb_dic['ny']
    nz = turb_dic['nz']
    sy = turb_dic['sy']
    sz = turb_dic['sz']
    zc = 44  # Hardcoded for V52
    df = dic['meas_stats']
    mast_pickle_path = turb_path + 'mast.p'
    ksec_bat_path = turb_path + 'ksec.bat'
    with open(mast_pickle_path, 'wb') as g:
        pickle.dump(df, g)
    if turb_constr == 0:
        con_type = 'unc'
    elif turb_constr == 1:
        con_type = 'con'
    else:
        raise ValueError(f'Unrecognized simulation type "{con_type}"')
    ksec_string = 'python {} {} {} {} {} {} {} {} {}'.format(
            ksec_simu_path, ny, nz, sy, sz, zc, con_type, mast_pickle_path,
            turb_dir)
    with open(ksec_bat_path, 'w') as g:
        g.write(ksec_string)
    os.system(ksec_string)


def get_turb_params_from_htc(htc_path):
    htc = HTCFile(htc_path)
    nx, dx = htc.wind.mann.box_dim_u[:]
    ny, dy = htc.wind.mann.box_dim_v[:]
    nz, dz = htc.wind.mann.box_dim_w[:]
    sx, sy, sz = [(nx-1)*dx, (ny-1)*dy, (nz-1)*dz]
    L, ae, Gamma, seed, highfrq_comp = htc.wind.mann.create_turb_parameters[:]
    turb_lst = [nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed]
    turb_lst = [str(x) for x in turb_lst]
    turb_string = ' '.join(turb_lst)
    turb_dic = {
            'nx': nx,
            'ny': ny,
            'nz': nz,
            'dx': dx,
            'dy': dy,
            'dz': dz,
            'sx': sx,
            'sy': sy,
            'sz': sz,
            'ae': ae,
            'L': L,
            'Gamma': Gamma,
            'seed': seed,
            'turb_string': turb_string,
            }
    return turb_dic


if __name__ == '__main__':
    hawc2_dir = r'C:\Sandbox\Git\V52\tjul\HAWC2'
    csimu2_path = r"C:\Sandbox\Git\V52\tjul\csimu2.exe"
    lc = 'sa_wsp_sa_ti'
    create_turb(hawc2_dir, csimu2_path, lc)
