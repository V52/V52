import hashlib
#from hashlib import blake2b, blake2s
import json

def get_hash(**kwargs):
    json_obj = json.dumps(kwargs, sort_keys=True).encode('utf-8')
    w = hashlib.md5(json_obj).hexdigest()
    n=8
    wlist = [w[i:i+n] for i in list(range(0, len(w), n))]
    return '-'.join(wlist)

def get_hash_short(**kwargs):
    json_obj = json.dumps(kwargs, sort_keys=True).encode('utf-8')
    w = hashlib.blake2b(json_obj,digest_size=5).hexdigest()
    n=5
    wlist = [w[i:i+n] for i in list(range(0, len(w), n))]
    return '_'.join(wlist)

if __name__ == "__main__":
    dic = {
        'a':432,
        'b':7465,
    }
    print(get_hash(**dic))
    print(get_hash(a=432,b=7465))
    print(get_hash_short(**dic))
    print(get_hash_short(a=432,b=7465))