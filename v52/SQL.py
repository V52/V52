import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime
try:
    import MySQLdb
    def SQLconnect(I):
        cnx = MySQLdb.connect(
                host = I['host'],
              user = I['user'],
              passwd = I['password'],
              db = I['database'])  
        return cnx
except:
    import mysql.connector
    def SQLconnect(I):
        cnx = mysql.connector.connect(
            user=I['user'],
            password=I['password'],
            host=I['host'],
            database=I['database'])
        return cnx



def SQLdataframe(cnx, table_name, limit='default', col='*', row='1=1', logical_statements=[]):
    if limit != 'default':
        string = "SELECT {:s} FROM {:s} WHERE {:s} LIMIT {:d}".format(
                col, table_name, row, limit)
    else:
        string = "SELECT {:s} FROM {:s} WHERE {:s}".format(
                col, table_name, row)
    if len(logical_statements)>0:
        string += ' AND ' + ' AND '.join(logical_statements)
#    print(string)
    df = pd.read_sql(
        string,
        con=cnx
        )
    return df


def SQL_read_table(I):
    cnx = SQLconnect(I)
    table_name = I['table_name']
    if 'cols' in I:
        if I['cols'] == 'all':
            cols = '*'
        else:
            cols = ','.join(I['cols'])
    else:
        cols = '*'
    if 'SQLrowlimit' in I and I['SQLrowlimit'] is not None:
        N = I['SQLrowlimit']
    else:
        N = 'default'
    if 'rows' in I:
        rows = I['rows']
        strings = []
        for row in rows:
            for ts in rows[row]:
                string = " `{}` LIKE '{}'".format(row, ts)
                strings.append(string)
        row = '(' + ' OR '.join(strings) + ')'
    else:
        row = '1=1'
    if 'logical_statements' in I:
        logical_statements = I['logical_statements']
    else:
        logical_statements = []
    df = SQLdataframe(cnx, table_name, limit=N, col=cols, row=row,
                      logical_statements=logical_statements)
    cnx.close()
    return df


def SQL_write_table(I, port=3306, if_exists='append'):
    string = "mysql+mysqldb://{}:{}@{}:{}/{}".format(
        I['user'],
        I['password'],
        I['host'],
        port,
        I['database'],
        )
    I.pop('SQLrowlimit', None)
    engine = create_engine(string)
    df = I['df']
    df.to_sql(I['table_name'], engine, if_exists=if_exists, index=False)


def sort_db(I):
    df = SQL_read_table(I)
    try:
        df = df.drop('index', axis=1)
    except Warning('df does not exist'):
        pass
    df['colFromIndex'] = df.index
    df = df.sort_values(['name', 'colFromIndex'])
    df = df.drop('colFromIndex', axis=1)
    df = df.reset_index(drop=True)
    I['df'] = df
    SQL_write_table(I, port=3306, if_exists='replace')


def delete_df_content(I):
    df = SQL_read_table(I)
    df_empty = df[0:0]
    I['df'] = df_empty
    SQL_write_table(I, port=3306, if_exists='replace')


def alter_column_name(I):
    table_name = I['table_name']
    old_name = I['old_name']
    new_name = I['new_name']
    database = I['database']
    sql = ''
    sql += 'ALTER TABLE `{}`.`{}` '.format(database, table_name)
    sql += 'CHANGE `{}` `{}` VARCHAR(255) NOT NULL;'.format(old_name, new_name)
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()


def clear_duplicates(I):
    database = I['database']
    table_name = I['table_name']
    unique_keys = I['unique_keys']
    group_by = ','.join(unique_keys)
    sql = ''
    sql += 'CREATE TABLE {}.table_temp AS '.format(database)
    sql += 'SELECT * FROM {}.{} GROUP BY {};'.format(database, table_name, group_by)
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()
    sql = ''
    sql += 'DROP TABLE {}.{};'.format(database, table_name)
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()
    sql = ''
    sql += 'RENAME TABLE {}.table_temp TO {}.{};'.format(database, database, table_name)   
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()


def create_Channel_names(I):
    sql = '''
CREATE TABLE `v52_simulation`.`channel_names` (
`chan_name` VARCHAR(24) NULL,
`in_use` ENUM('F', 'T') NULL,
PRIMARY KEY (`chan_name`))
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_general_ci;
'''
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()

def fill_Channel_names(I):
    df = SQL_read_table(I)
    chan_name = list(df)[3:]
    in_use = len(chan_name)*['T']
    df2 = pd.DataFrame({
            'chan_name': chan_name,
            'in_use': in_use,
                })
    I = {
        'user': 'mikf',
        'password': password.pass_sql,
        'host': '10.40.20.14',
        'database': 'v52_simulation',
        'table_name': 'channel_names',
        'df': df2,
        }
    SQL_write_table(I, if_exists='replace')


def create_channel_spec(I):
    sql = '''
    CREATE TABLE `v52_simulation`.`channel_spec` (
  `list_version` bigint(20) NOT NULL,
  `list_index` bigint(20) NOT NULL,
  `chan_index` bigint(20) DEFAULT NULL,
  `chan_name` varchar(24) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `height_m` float DEFAULT NULL,
  `changed` enum('F','T') COLLATE latin1_general_ci DEFAULT NULL,
  `new` enum('F','T') COLLATE latin1_general_ci DEFAULT NULL,
  `frequency` float DEFAULT NULL,
  `fast_saved` enum('F','T') COLLATE latin1_general_ci NOT NULL DEFAULT 'F',
  `instr_make` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `instr_type` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `instr_sn` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `instr_cal_A` float DEFAULT NULL,
  `instr_cal_B` float DEFAULT NULL,
  `instr_cal_C` float DEFAULT NULL,
  `cond_make` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `cond_type` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `cond_sn` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `line_gain` float DEFAULT NULL,
  `line_offset` float DEFAULT NULL,
  `logger_id` bigint(20) DEFAULT NULL,
  `structure_id` bigint(20) DEFAULT NULL,
  `location` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `boom_length_m` float DEFAULT NULL,
  `boom_dir` float DEFAULT NULL,
  `height_above_boom_m` float DEFAULT NULL,
  `top_mount` enum('F','T') COLLATE latin1_general_ci NOT NULL DEFAULT 'F',
  `missing_data` float DEFAULT NULL,
  `variable_id` bigint(20) DEFAULT NULL,
  `status_comment` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `setup_comment` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  '''
    lst = ['azimuth',
     'flap_nr',
     'output_type',
     'io_nr',
     'pos',
     'io',
     'sensortype',
     'bodyname',
     'sensortag',
     'bearing_name',
     'blade_nr',
     'component',
     'dll',
     'coord',
     'direction',
     'units',
     'radius',
     'unique_ch_name']
  
    sql += '`' + '` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,\n`'.join(lst)
    sql += '` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,\n'
    sql += '''
  PRIMARY KEY (`list_version`,`list_index`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
    '''
    print(sql)
    cnx = SQLconnect(I)
    mycursor = cnx.cursor()
    mycursor.execute(sql)
    cnx.commit()
   
    
def fill_channel_spec(I,meta_data):
    import numpy as np
    meta_data = meta_data.T
    df = SQL_read_table(I)
    chan_name = list(df)[3:]
    l = len(chan_name)
    list_version = l*[1]
    list_index = np.arange(1,l+1)
    chan_index = list_index
    dic = {
            'list_version': list_version,
            'list_index': list_index,
            'chan_index': chan_index,
            'chan_name': chan_name,
                }
    lst = ['azimuth',
     'flap_nr',
     'output_type',
     'io_nr',
     'pos',
     'io',
     'sensortype',
     'bodyname',
     'sensortag',
     'bearing_name',
     'blade_nr',
     'component',
     'dll',
     'coord',
     'direction',
     'units',
     'radius',
     'unique_ch_name']
    for l in lst:
        dic[l] = meta_data[l].values[4:]
    df3 = pd.DataFrame(dic)
    I = {
        'user': 'mikf',
        'password': password.pass_sql,
        'host': '10.40.20.14',
        'database': 'v52_simulation',
        'table_name': 'channel_spec',
        'df': df3,
        }
    SQL_write_table(I, if_exists='replace')


def fill_channel_specifications(I):
    dic = {
            'list_version':[1],
            'valid_from':[datetime(2018, 1, 1, 0, 0, 0)],
            'list_name': ['V52sim ver1'],
            'time_zone': ['UTC+0100'],
            }
    df = pd.DataFrame(dic)
    I = {
        'user': 'mikf',
        'password': password.pass_sql,
        'host': '10.40.20.14',
        'database': 'v52_simulation',
        'table_name': 'channel_specifications',
        'df': df,
        }
    SQL_write_table(I, if_exists='replace')

if __name__ == '__main__':
    import password
    user = password.user
    password = password.pass_sql
    I = {
        'table_name': 'caldata_2018_03_50hz',
        'user': user,
        'password': password,
        'database': 'v52_wtg',
        'host': '10.40.20.14',
        }
    cnx = SQLconnect(I)
#    cnx = SQLconnect2(I)

#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` LIKE '201803010100'"
#    startTime = datetime.now()
#    df = pd.read_sql(string, con=cnx)
#    print(datetime.now() - startTime)
##    0:00:22.675397

#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` >= '201803010100' AND `Name` <= '201803010100'"
#    startTime = datetime.now()
#    df = pd.read_sql(string, con=cnx)
#    print(datetime.now() - startTime)
##    0:00:23.207084


#    string = "SELECT * FROM caldata_2018_03_50hz WHERE FIND_IN_SET(`Name`,'201803010100')"
#    startTime = datetime.now()
#    df = pd.read_sql(string, con=cnx)
#    print(datetime.now() - startTime)
##    Impossible

#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` LIKE '201803010100' OR `Name` LIKE '201803010110' OR `Name` LIKE '201803010120'"
#    startTime = datetime.now()
#    df = pd.read_sql(string, con=cnx)
#    print(datetime.now() - startTime)
##    0:01:08.708687

#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` >= '201803010100' AND `Name` <= '201803010120'"
#    startTime = datetime.now()
#    df = pd.read_sql(string, con=cnx)
#    print(datetime.now() - startTime)
##    0:01:10.284159


#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` LIKE '201803010100' OR `Name` LIKE '201803010110' OR `Name` LIKE '201803010120'"
#    startTime = datetime.now()
#    mycursor = cnx.cursor()
#    mycursor.execute(string)
#    myresult = mycursor.fetchall()
#    print(datetime.now() - startTime)
##    0:00:56.604824
    
#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` >= '201803010100' AND `Name` <= '201803010120'"
#    startTime = datetime.now()
#    mycursor = cnx.cursor()
#    mycursor.execute(string)
#    myresult = mycursor.fetchall()
#    print(datetime.now() - startTime)
##    0:00:54.983104
    
    
#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` >= '201803010100' AND `Name` <= '201803010120'"
#    cnx = SQLconnect2(I)
#    startTime = datetime.now()   
#    cursor = cnx.cursor()
#    cursor.execute (string)
#    myresult = cursor.fetchall()
#    print(datetime.now() - startTime)
##    0:00:36.591643
    
    
#    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` LIKE '201803010100' OR `Name` LIKE '201803010110' OR `Name` LIKE '201803010120'"
#    startTime = datetime.now()   
#    cursor = cnx.cursor()
#    cursor.execute (string)
#    myresult = cursor.fetchall()
#    print(datetime.now() - startTime)
##    0:00:35.850298
    
    startTime = datetime.now()   
    string = "SELECT * FROM caldata_2018_03_50hz WHERE `Name` LIKE '201803010100' OR `Name` LIKE '201803010110' OR `Name` LIKE '201803010120'"
    df = pd.read_sql(
        string,
        con=cnx
        )
    print(datetime.now() - startTime)
