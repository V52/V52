from wetb.hawc2.htc_file import HTCFile
import numpy as np


def run_script(htc_path, LC, data, ts):
    htc = HTCFile(htc_path)
    df = data['calmeans'][data['calmeans'].Name == ts]
    dfstd = data['calstdvs'][data['calstdvs'].Name == ts]

    htc.wind.mann.create_turb_parameters = seed(df, ts, htc)
    sim_time = htc.simulation.time_stop[0] - htc.wind.scale_time_start[0]
    htc.wind.shear_format = shear(df)
    htc.wind.wsp = sonic_anemometer_wsp(df)
    htc.wind.tint = turbulence_intensity_basic(df, dfstd)
    wsp = htc.wind.wsp[0]
    box_points_u = htc.wind.mann.box_dim_u[0]
    du = sim_time*wsp/(box_points_u-1)
    htc.wind.mann.box_dim_u = box_points_u, du
    htc.wind.mann.filename_u = "./turb/" + LC['name'] + "/" + ts + "_u.bin"
    htc.wind.mann.filename_v = "./turb/" + LC['name'] + "/" + ts + "_v.bin"
    htc.wind.mann.filename_w = "./turb/" + LC['name'] + "/" + ts + "_w.bin"
    htc.simulation.logfile = "./logfiles/" + LC['name'] + "/" + ts + '.log'
    
    htc.output.filename = './res/' + LC['name'] + "/" + ts
    htc.save()


def seed(df, time_stamp, htc_obj):
    (L, ae, Gamma, seed,
     highfrq_comp) = htc_obj.wind.mann.create_turb_parameters[:]
    wsp = np.mean(df.SWsp_44m)
    fh = round(wsp*10, 0)*10
    sh = int(str(time_stamp)[-2:-1])
    seed = fh + sh   # make sure it is four digits
    seed = '{:04d}'.format(int(seed))
    value = (L, ae, Gamma, seed, highfrq_comp)
    return value


def wsp(df, time_stamp, htc_obj):
    htc_entry = 'wind.wsp'
    wsp = df.SWsp_44m
    value = np.nanmean(wsp)
    return htc_entry, value


def shear(df):
    '''
    Basic power-law shear based on met mast cup anemometer
    '''
    from wetb.wind.shear import fit_power_shear_ref
    z_lst = [70, 57, 44, 31, 18]
    u_lst = [df["Wsp_%dm" % z][:] for z in z_lst]
    alpha, u = fit_power_shear_ref(list(zip(z_lst, u_lst)), 44)
    value = 3, alpha
    return value


def turbulence_intensity_basic(df, dfstd):
    '''
    Basic turbulence intensity calculation based on the met mast cup
    anemometer.
    '''
    value = float(dfstd.sa_speed/df.sa_speed)
    return value


def sonic_anemometer_wsp(df):
    wsp = df.sa_speed*1.445
    wsp = np.nanmean(wsp)
    value = wsp
    return value


if __name__ == '__main__':
    htc_path = r"C:\Sandbox\Git\V52\temp\HAWC2\htc\LC1_wsp\201801010100.htc"
    LC = {
            'name': 'LC1_wsp',
            'script': r"C:\Sandbox\Git\V52\temp\LCs\LC1_wsp.py"}
    data_path = r"C:\Sandbox\Git\V52\v52\data3.p"
    ts = '201801010100'
    import pickle
    with open(data_path, 'rb') as f:
        data = pickle.load(f)
    run_script(htc_path, LC, data, ts)
