# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 10:30:13 2018

@author: mikf
"""
import os
import matplotlib.pyplot as plt
import pickle
from wetb.hawc2.htc_file import HTCFile
import numpy as np

def get_zt(zm, HH, nz, lz):
    zt = int(zm-HH+(nz-1)*lz/2)
    return zt


def get_index_from_x(f, x, wsp, ts, tm):
    ind = int(f*(x/wsp-(ts-tm)))
    return ind


def l_to_grid(d, l):
    return int(l/d+1)


def create_constr(constr_path, N, x, y, z, comp, wsp):
    '''
    Creates the constr.dat-file to use with csim2.exe based on input on
    no. of constraints, windspeed and constrained component location.
    '''
    c = ''
    for i in range(N):
        lst = [x[i], y[i], z[i], comp[i], wsp[i]]
        c += ';'.join([str(l) for l in lst])
        if not i == N-1:
            c += '\n'
    with open(constr_path, 'w') as f:
        f.write(c)


def create_csim_bat(turb_dir, csim_exe_path, csim_bat_path, nx, ny, nz, sx, sy, sz, ae,
                    L, Gamma, seed, constrained=True):
    '''
    Creates the *.bat file that executes the csim2.exe program with relevant
    arguments based on the input given.
    '''
    lst = [nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed]
    lst = [str(l) for l in lst]
    ID = '_'.join(lst)
    constr = os.path.join(turb_dir, 'constr.dat')
    fn = os.path.join(turb_dir, ID)
    lst = [csim_exe_path] + lst
    if constrained:
        lst += [fn, constr]
    else:
        lst += [fn]
    string = ' '.join(lst)
    with open(csim_bat_path, 'w') as f:
        f.write(string)

def run_plots(calc_dir, force=False):
    '''
    Creates Plots of specified components of the generated turbulence box.
    '''
    for root,dirs,files in os.walk(calc_dir):
        for f in files:
            if f == 'input.p':
                folder_path = root
                base_name = os.path.basename(root)
                plot_path = os.path.join(folder_path,
                                            'plot_{}.png'.format(base_name))
                if os.path.exists(plot_path) and not force:
                    pass
                else:
                    path = os.path.join(root,f)
                    plot_pickle_path = os.path.join(folder_path,'plot.p')
                    pdap_script_path = os.path.join(folder_path,'plt_res.py')
                    with open(path,'rb') as g:
                        dic = pickle.load(g)
                    xs = list(dic['xs'])
                    WSP = list(dic['WSP'])
#                    lst = os.listdir(os.path.join(root,'turb'))
                    lst = os.listdir(os.path.join(root))
                    for l in lst:
                        if l.endswith('u.bin'):
                            ubin_name = l
                    if len(lst) == 0:
                        break
                    _write_pdap_term_script(folder_path,ubin_name,dic,
                                            plot_pickle_path,pdap_script_path)
                    _run_pdap_terminal(r'C:\Program Files (x86)\Pdap-3.14.0.win-amd64\Pdap-3.14.0.win-amd64\PdapTerminal.exe',
                            pdap_script_path
                    )
                    with open(plot_pickle_path,'rb') as i:
                        ds = pickle.load(i)
                    for key in ds:
                        plt.plot(ds[key],'--')
#                    plt.plot(ds[16])
#                    plt.plot(ds)
                    plt.scatter(xs,WSP,color='red')
                    plt.xlim([1150,1200])
                    plt.savefig(plot_path)
                    plt.close()

def _write_pdap_term_script(folder_path,ubin_name,dic,
                            plot_pickle_path,pdap_script_path):
    '''
    Writes a Pdap script to open the turbulence file
    '''
    nx,ny,nz = dic['nx'],dic['ny'],dic['nz']
#    out_name = os.path.join(folder_path,'res.png')
    sx,sy,sz = dic['sx'],dic['sy'],dic['sz']
#    y,z = 8,8
    c = ''    
    c += 'folder= r"{}/"\n'.format(folder_path)
    c += 'ds  = OpenTurbulence(filename=folder + "{}",\n'.format(ubin_name)
    c += '                dim=({},{},{}),\n'.format(nx,ny,nz)
    c += '                distance=({},{},{}), \n'.format(sx,sy,sz)
    c += '                transport_speed=0, \n'
    c += '                mean_wsp=0, \n'
    c += '                center_position=(0, 0, -100))\n'
    c += 'data = {}\n'
    c += 'y = {}\n'.format(dic['y'])
    c += 'zs = {}\n'.format(dic['zs'])
    c += 'for z in zs:\n'
    c += "    data[z] = ds('(x,{},{})'.format(y,z))*1.0\n"
#    c += "data = ds('(x,{},{})')\n".format(y,z)
    c += 'import pickle\n'
    c += 'pickle.dump(data,open(r"{}","wb"))\n'.format(plot_pickle_path)
    with open(pdap_script_path,'w') as h:
        h.write(c)
#
def _run_pdap_terminal(pdap_terminal_path,script_path):
    '''
    Executes a Pdap script using the Pdap Terminal
    '''
    string = '""{}" "{}""'.format(pdap_terminal_path,script_path)
    os.system(string)
    
def dump_pickle(lst,lst_str,dic,folder_path):
    '''
    Dumps essential input as a pickle file in the current folder for a 
    specific turbulense box simulation
    '''
    for k,v in zip(lst_str,lst):
        dic[k] = v
    pickle_path = os.path.join(folder_path,'input.p')
    with open(pickle_path,'wb') as f:
        pickle.dump(dic,f)

def create_conturb_input_mann(csim_exe_path, htc_path, data, ts,
                                LC_name='conturb_mann', wsp=None,
                                sim_time=None, meas_time=None, hub_height=None,
                                shear=None):
    if not wsp:
        wsp = data.Wsp_44m.mean()
    htc = HTCFile(htc_path)
    if not sim_time:
        sim_time = htc.simulation.time_stop[0]
    if not meas_time:
        meas_time = 600
    if not hub_height:
        hub_height = 44
    nx = htc.wind.mann.box_dim_u[0]
    sx = sim_time*wsp
    dx = sx/(nx-1)
    ny, dy = htc.wind.mann.box_dim_v[:]
    sy = (ny-1)*dy
    nz, dz = htc.wind.mann.box_dim_w[:]
    sz = (nz-1)*dz
    f = 50
    (L, ae, Gamma, seed,
     highfrq_comp) = htc.wind.mann.create_turb_parameters[:]
    dist = 100
    
    # create individual arrays
    M_time = np.array([int(i) for i in np.arange(50, 600, 50)])
    T_time = M_time+(sim_time-meas_time)-dist/wsp
    T_xL = T_time*wsp
#    print(dx)
#    print)
    T_xG = [l_to_grid(dx,i) for i in T_xL]
    T_yL = 31
    T_yG = l_to_grid(dy, T_yL)
    M_ind = f*M_time
    meas = ['Wsp_70m',
            'Wsp_57m',
            'Wsp_44m',
            'Wsp_31m',
            'Wsp_18m']
    M_zL = np.array([int(i[-3:-1]) for i in meas])
    T_zL = np.array([get_zt(zm, hub_height, nz, dz) for zm in M_zL])
    T_zG = [l_to_grid(dz, i) for i in T_zL]
    
    # mesh arrays together
    yy, xx, zz = np.meshgrid(T_yG, T_xG, T_zG)
    x = xx.flatten()
    y = yy.flatten()
    z = zz.flatten()
    ind_x = [np.where(i==T_xG)[0] for i in x]
    M_ind_new = M_ind[ind_x].flatten()
    ind_z = [np.where(i==T_zG)[0] for i in z]
    z_new = M_zL[ind_z].flatten()
    if not shear:
        shear = np.zeros_like(x)
    us = np.array([data['Wsp_{:d}m'.format(i)].values[j] for i, j in zip(z_new, M_ind_new)]) - wsp - shear
    N = len(x)
    return x, y, z, us, N, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, wsp

def get_ID(nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed):
    lst = [nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed]
    lst = [str(l) for l in lst]
    ID = '_'.join(lst)
    return ID


def update_htc(htc_path, ID, ts, wsp, LC_name='conturb_mann'):
    htc = HTCFile(htc_path)
    htc.wind.wsp = wsp
    htc.wind.mann.filename_u = "./turb/" + LC_name + "/" + ts + "/" + ID + "u.bin"
    htc.wind.mann.filename_v = "./turb/" + LC_name + "/" + ts + "/" + ID + "v.bin"
    htc.wind.mann.filename_w = "./turb/" + LC_name + "/" + ts + "/" + ID + "w.bin"
    htc.simulation.logfile = "./logfiles/" + LC_name + "/" + ts + '.log'
    htc.output.filename = './res/' + LC_name + "/" + ts
    htc.save()

def run_conturb(hawc2_dir):
    for root,dirs,files in os.walk(hawc2_dir):
        for f in files:
            if f == 'constr.bat':
                path = os.path.join(root, f)
                os.system(path)
                

if __name__ == '__main__':
    csim_exe_path = r"C:\Sandbox\ConTurb\Conturb\csimu2_win\csimu2.exe"
    htc_path = r"C:\Sandbox\Git\V52\examples\example_4\HAWC2\htc\LC1_wsp\201802010100.htc"
    data_path = r"C:\Sandbox\Git\V52\examples\example_4\data5.p"
    ts = '201802010100'
#    calc_dir = r"C:\Sandbox\Git\V52\turb"
    hawc2_dir = r"C:\Sandbox\Git\V52\examples\example_4\HAWC2"
    LC_name='conturb_mann'
    turb_dir = os.path.join(hawc2_dir,'turb',LC_name,ts)
    if not os.path.exists(turb_dir):
        os.makedirs(turb_dir)

    with open(data_path, 'rb') as f:
        data = pickle.load(f)
    data = data['caldata_{}_{}_50hz']
    data = data[data.Name == ts]
    x, y, z, us, N, nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed, wsp = create_conturb_input_mann(csim_exe_path, htc_path, data, ts)
    ID = get_ID(nx, ny, nz, sx, sy, sz, ae, L, Gamma, seed)
    constr_path = os.path.join(turb_dir, 'constr.dat')
    csim_bat_path = os.path.join(turb_dir, 'constr.bat')
    create_constr(constr_path, N, x, y, z, N*['u'], us)
    create_csim_bat(turb_dir, csim_exe_path, csim_bat_path, nx, ny, nz, sx, sy, sz, ae,
                    L, Gamma, seed, constrained=True)
    update_htc(htc_path, ID, ts, wsp)
    run_conturb(hawc2_dir)
#    comps = ['u','v','w']
#    files = [ID+c+'.bin' for c in comps]
#    lst = [1,nx,ny,nz,seed,N,1]
#    lst_str = ['RN','nx','ny','nz','sn','noc','n']
#    ID = '_'.join([str(l) for l in lst])
#    dic = {
#        'ID':ID,
#        'xs':xg,
#        'WSP':us,
#        'sx': sx,
#        'sy': sy,
#        'sz': sz,
#        'y': 16,
#        'zs': zgs,
#        }
#    folder_path = os.path.join(calc_dir)
##    repo = os.path.join(os.path.dirname(calc_dir),'file_repo')
##    create_folder(folder_path,repo)
#    dump_pickle(lst,lst_str,dic,folder_path)
#    run_plots(calc_dir, force=True)