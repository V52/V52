# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 14:10:13 2018

@author: mikf
"""
from v52 import V52
from datetime import datetime
import sched
import time
import password

calc_dir = r"C:\Sandbox\Git\V52\auto"
v52 = V52(calc_dir=calc_dir)
start = (2018, 10, 10, 1, 0)
LC1_script_path = r"C:\Sandbox\Git\V52\temp\LCs\LC1_wsp.py"
s = sched.scheduler(time.time, time.sleep)
LCs = [{'name': 'LC1_wsp', 'script': LC1_script_path}]
tabelnames = ['calmeans', 'calstdvs']
user = password.user
pass_gorm = password.pass_gorm
password = password.pass_sql
v52.get_model()


def create_htc(sc):
    now = datetime.now()
    print(now)
    stop = (now.year, now.month, now.day, now.hour, int(now.minute/10)*10)
    v52.get_time_series(start, stop)
    ts_requested = v52.time_series
    v52.get_data_from_SQL(user, password, v52.time_series,
                          tablename=tabelnames[0])
    v52.get_time_series_from_data(tabelnames[0])
    ts_available = v52.time_series
    print('Number of 10-minute series available: {}'.format(len(ts_available)))
    v52.init_LCs(LCs)
    ts_done = v52.get_completed_htcs()
    print('Number of 10-minute series done: {}'.format([len(lst) for lst in list(ts_done.values())]))
    counter = 0
    for ts in ts_available:
        if ts in ts_requested:
            for LC in v52.info['LCs']:
                if ts not in ts_done[LC['name']]:
                    counter += 1
    if counter > 0:
        print('Unresolved load cases found: {}'.format(counter))
        for tablename in tabelnames:
            v52.get_data_from_SQL(user, password, v52.time_series,
                                  tablename=tablename)
        v52.get_time_series_from_data(tabelnames[0])
        htcs_created = v52.resolve_LCs(LCs)
        print('New htc-files created: {}'.format(len(htcs_created)))
    else:
        print('Nothing new to create')
    s.enter(5, 1, create_htc, (sc,))


s.enter(5, 1, create_htc, (s,))
s.run()
