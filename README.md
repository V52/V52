[![pipeline status](https://gitlab.windenergy.dtu.dk/V52/V52/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/V52/V52/commits/master)
[![coverage status](https://gitlab.windenergy.dtu.dk/V52/V52/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/V52/V52/commits/master)


Welcome to V52
------------------

This is a tool for comparing measurements and simulations and is being developed by DTU Wind
Energy.

# Installation Guide, Examples, and Documentation

This tool requires Wind Energy Toolbox: https://gitlab.windenergy.dtu.dk/toolbox/WindEnergyToolbox 

Installation:
```
git clone https://gitlab.windenergy.dtu.dk/V52/V52.git
cd V52
pip install -e .
```